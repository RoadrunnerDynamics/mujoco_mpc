# Copyright 2021 DeepMind Technologies Limited
# Copyright 2023 Alec Graves
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set(BUILD_SHARED_LIBS_OLD ${BUILD_SHARED_LIBS})
set(BUILD_SHARED_LIBS
    OFF
    CACHE INTERNAL "Build SHARED libraries"
)

# lodepng is non-cmake; manually add target
set(lodepng_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/lodepng)
set(LODEPNG_SRCS ${lodepng_SOURCE_DIR}/lodepng.cpp)
set(LODEPNG_HEADERS ${lodepng_SOURCE_DIR}/lodepng.h)
add_library(lodepng STATIC ${LODEPNG_HEADERS} ${LODEPNG_SRCS})
target_compile_options(lodepng PRIVATE ${MUJOCO_MACOS_COMPILE_OPTIONS})
set_target_properties(lodepng PROPERTIES LINK_FLAGS "${MUJOCO_MACOS_LINK_OPTIONS}")

install(FILES ${LODEPNG_HEADERS}
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

target_include_directories(lodepng PUBLIC
    $<BUILD_INTERFACE:${lodepng_SOURCE_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

set(QHULL_ENABLE_TESTING OFF)
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/qhull" "${CMAKE_BINARY_DIR}/_deps/qhull")

# MuJoCo includes a file from libqhull_r which is not exported by the qhull include directories.
# Add it to the target.
target_include_directories(
  qhullstatic_r INTERFACE $<BUILD_INTERFACE:${qhull_SOURCE_DIR}/src/libqhull_r>
)

target_compile_options(qhullstatic_r PRIVATE ${MUJOCO_MACOS_COMPILE_OPTIONS})
set_target_properties(qhullstatic_r PROPERTIES LINK_FLAGS "${MUJOCO_MACOS_LINK_OPTIONS}")

set(tinyxml2_BUILD_TESTING OFF)
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/tinyxml2" "${CMAKE_BINARY_DIR}/_deps/tinyxml2")
target_compile_options(tinyxml2 PRIVATE ${MUJOCO_MACOS_COMPILE_OPTIONS})
set_target_properties(tinyxml2 PROPERTIES LINK_FLAGS "${MUJOCO_MACOS_LINK_OPTIONS}")

add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/tinyobjloader" "${CMAKE_BINARY_DIR}/_deps/tinyobjloader")

set(ENABLE_DOUBLE_PRECISION ON)
set(CCD_HIDE_ALL_SYMBOLS ON)
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/ccd" "${CMAKE_BINARY_DIR}/_deps/ccd")
target_compile_options(ccd PRIVATE ${MUJOCO_MACOS_COMPILE_OPTIONS})
set_target_properties(ccd PROPERTIES LINK_FLAGS "${MUJOCO_MACOS_LINK_OPTIONS}")

# libCCD has an unconditional `#define _CRT_SECURE_NO_WARNINGS` on Windows.
# TODO(stunya): Remove this after https://github.com/danfis/libccd/pull/77 is merged.
if(WIN32)
  if(MSVC)
    # C4005 is the MSVC equivalent of -Wmacro-redefined.
    target_compile_options(ccd PRIVATE /wd4005)
  else()
    target_compile_options(ccd PRIVATE -Wno-macro-redefined)
  endif()
endif()

set(ABSL_PROPAGATE_CXX_STD ON)

# This specific version of Abseil does not have the following variable. We need to work with BUILD_TESTING
set(BUILD_TESTING_OLD ${BUILD_TESTING})
set(BUILD_TESTING
    OFF
    CACHE INTERNAL "Build tests."
)

set(ABSL_BUILD_TESTING OFF)
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/abseil" "${CMAKE_BINARY_DIR}/_deps/abseil")


set(BUILD_TESTING
    ${BUILD_TESTING_OLD}
    CACHE BOOL "Build tests." FORCE
)

# Avoid linking errors on Windows by dynamically linking to the C runtime.
set(gtest_force_shared_crt
    ON
    CACHE BOOL "" FORCE
)

add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/gtest" "${CMAKE_BINARY_DIR}/_deps/gtest")

set(BENCHMARK_EXTRA_FETCH_ARGS "")
if(WIN32 AND NOT MSVC)
  set(BENCHMARK_EXTRA_FETCH_ARGS
      PATCH_COMMAND
      "sed"
      "-i"
      "-e"
      "s/-std=c++11/-std=c++14/g"
      "-e"
      "s/HAVE_CXX_FLAG_STD_CXX11/HAVE_CXX_FLAG_STD_CXX14/g"
      "${CMAKE_BINARY_DIR}/_deps/benchmark-src/CMakeLists.txt"
  )
endif()

set(BENCHMARK_ENABLE_TESTING OFF)

add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/benchmark" "${CMAKE_BINARY_DIR}/_deps/benchmark")

add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/eigen3" "${CMAKE_BINARY_DIR}/_deps/eigen3")

# Reset BUILD_SHARED_LIBS to its previous value
set(BUILD_SHARED_LIBS
    ${BUILD_SHARED_LIBS_OLD}
    CACHE BOOL "Build MuJoCo as a shared library" FORCE
)
