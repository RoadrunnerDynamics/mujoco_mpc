#!/bin/sh
set -e
sudo dnf install libXrandr-devel libXinerama-devel libXcursor-devel libXi-devel mesa-libGL-devel
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=On
make -j $(( $(nproc) - 1 ))
